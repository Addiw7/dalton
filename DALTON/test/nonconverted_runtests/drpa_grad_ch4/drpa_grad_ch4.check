#!/bin/ksh
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

# Nuclear repulsion
CRIT1=`$GREP "Nuclear repulsion: * 16\.91009781024" $log | wc -l`
TEST[1]=`expr $CRIT1 `
CTRL[1]=1
ERROR[1]="NUCLEAR REPULSION NOT CORRECT"

# HF energy
CRIT1=`$GREP "Final * HF energy: * \-40\.022534846" $log | wc -l`
TEST[2]=`expr $CRIT1 `
CTRL[2]=1
ERROR[2]="HF ENERGY NOT CORRECT"

# SOSEX ENERGY
CRIT1=`$GREP "Total SOSEX Energy: * \-40\.202114455" $log | wc -l`
CRIT2=`$GREP "SOSEX Correlation Energy: * (\-0|\-).179579609" $log | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2 `
CTRL[3]=4
ERROR[3]="SOSEX ENERGIES NOT CORRECT"

# dRPA energy
CRIT1=`$GREP "Total DRCCD Energy: * \-40\.319537005" $log | wc -l`
CRIT2=`$GREP "DRCCD Correlation Energy: * (\-0|\-).297002159" $log | wc -l`
TEST[4]=`expr $CRIT1 \+ $CRIT2 `
CTRL[4]=4
ERROR[4]="DRPA ENERGIES NOT CORRECT"

# dRPA solution stabilizing (Hurwitz check)
CRIT1=`$GREP "Solution is stabilizing" $log | wc -l`
TEST[5]=`expr $CRIT1`
CTRL[5]=1
ERROR[5]="NOT STABILIZING SOLUTION"

# dRPA dipole moment (relaxed)
CRIT1=`$GREP "   x * (0| |\-0|\-)\.00000000.*(0| |\-0|\-)\.00000000.*(0| |\-0|\-)\.00000000" $log | wc -l`
CRIT2=`$GREP "   y * (0| |\-0|\-)\.00000000.*(0| |\-0|\-)\.00000000.*(0| |\-0|\-)\.00000000" $log | wc -l`
CRIT3=`$GREP "   z * (0| |\-0|\-)\.00000000.*(0| |\-0|\-)\.00000000.*(0| |\-0|\-)\.00000000" $log | wc -l`
TEST[6]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 `
CTRL[6]=3
ERROR[6]="DRPA DIPOLE MOMENT NOT CORRECT"

# dRPA gradient
CRIT1=`$GREP " C * (0| |\-0|\-)\.00000000.*(0| |\-0|\-)\.00000000.*(0| |\-0|\-)\.00000000" $log | wc -l`
CRIT2=`$GREP " H1 * (\-0|\-)\.16199436.*(\-0|\-)\.16199436.*(\-0|\-).16199436" $log | wc -l`
CRIT3=`$GREP " H2 * (0| )\.16199436.*(0| )\.16199436.*(\-0|\-).16199436" $log | wc -l`
CRIT4=`$GREP " H3 * (\-0|\-)\.16199436.*(0| )\.16199436.*(0| ).16199436" $log | wc -l`
CRIT5=`$GREP " H4 * (0| )\.16199436.*(\-0|\-)\.16199436.*(0| ).16199436" $log | wc -l`
TEST[7]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 `
CTRL[7]=5
ERROR[7]="DRPA GRADIENT NOT CORRECT"

PASSED=1
for i in 1 2 3 4 5 6 7
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi
