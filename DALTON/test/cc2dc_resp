#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi

#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > cc2dc_resp.info <<%EOF%
   cc2dc_resp  
   --------------
   Molecule:         H2O in a spherical cavity of radius 4.00 au
   Wave Function:    CC2 / cc-pVDZ
   Test Purpose:     Check energy, dipole moment and static 
                     polarizability. 
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > cc2dc_resp.mol <<%EOF%
BASIS
cc-pVDZ
H2O i H2O(DC)
------------------------
    2    0         1 1.00D-12
        8.0   1
O            0.000000        0.000000        0.000000
        1.0   2
H           -0.756799        0.000000        0.586007
H            0.756799        0.000000        0.586007
%EOF%
#
#######################################################################
#  DALTON INPUT
#######################################################################
cat > cc2dc_resp.dal <<%EOF%
**DALTON INPUT
.RUN WAVEFUNCTION
**INTEGRALS
.DIPLEN
*ONEINT
.SOLVENT
 10
**WAVE FUNCTIONS
.CC
*SCF INPUT
.THRESH
1.0D-11
*CC INP
.CC2
.THRLEQ
 1.0D-7
.THRENR
 1.0D-7
.MAX IT
 90
.MXLRV
 60
*CCSLV
.SOLVAT
 1
10 4.00  78.54  1.778
.ETOLSL
 1.0D-6
.TTOLSL
 1.0D-6
.LTOLSL
 1.0D-6
.MXSLIT
 60
*CCFOP
.DIPMOM
.NONREL
*CCLR
.DIPOLE
.OLD_LR
**END OF DALTON INPUT
%EOF%
#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >cc2dc_resp.check
cat >>cc2dc_resp.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

# Total energy and solvation energy compared:
CRIT1=`$GREP "CC2      Total energy:                 -76\.2386472." $log | wc -l`
CRIT2=`$GREP "CC2      Solvation energy:              ( \-|\-0)\.0076650." $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2`
CTRL[1]=2
ERROR[1]="ENERGY TERMS NOT CORRECT"

# Dipole moment components compared:
CRIT1=`$GREP "z * ( |0)\.84595673 * 2\.15020740" $log | wc -l`
TEST[2]=`expr $CRIT1`
CTRL[2]=1
ERROR[2]="DIPOLE MOMENT NOT CORRECT"


# The component of the static polarizability tensor compared:
CRIT1=`$GREP "1 * 7\.140057.. * \-*0*\.0000000. * \-*0*\.0000000." $log | wc -l`
CRIT2=`$GREP "2 * \-*0*\.0000000. *  3\.213171.. * \-*0*\.0000000." $log | wc -l`
CRIT3=`$GREP "3 * \-*0*\.0000000. * \-*0*\.0000000. * 5\.320273.." $log | wc -l`
TEST[3]=`expr $CRIT1 + $CRIT2 + $CRIT3`
CTRL[3]=3
ERROR[3]="STATIC POLARIZABILITY NOT CORRECT"

PASSED=1
for i in 1 2 3
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

%EOF%
chmod +x cc2dc_resp.check
#######################################################################
